import { Component, OnInit } from '@angular/core';
import { Service1Service } from 'src/app/allservices/service1.service';

@Component({
  selector: 'app-component1',
  templateUrl: './component1.component.html',
  styleUrls: ['./component1.component.css']
})
export class Component1Component implements OnInit{

  service1_var :any;

  constructor(private service1 : Service1Service){

  }

  ngOnInit() {
   this.service1_var = this.service1.getService();
  }

}
