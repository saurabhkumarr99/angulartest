import { Component, OnInit } from '@angular/core';
import { Service2Service } from 'src/app/allservices/service2.service';

@Component({
  selector: 'app-component2',
  templateUrl: './component2.component.html',
  styleUrls: ['./component2.component.css']
})
export class Component2Component implements OnInit{

  service2_var :any;

  constructor(private service2 : Service2Service){

  }

  ngOnInit() {
   this.service2_var = this.service2.getService();
  }

}