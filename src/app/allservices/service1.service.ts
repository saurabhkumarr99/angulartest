import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Service1Service {

  service1_var :any;
  constructor() { 
    this.service1_var ='This is service one';
  }

  getService(){
    return this.service1_var;
  }
}
