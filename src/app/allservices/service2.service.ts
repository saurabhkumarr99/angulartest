import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Service2Service {

  service2_var :any;
  constructor() { 
    this.service2_var ='This is service two';
  }

  getService(){
    return this.service2_var;
  }
}
