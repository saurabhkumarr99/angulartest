import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { ThirdComponentComponent } from './third-component/third-component.component';
import { FourthComponent } from './fourth/fourth.component';
import { MybranchComponent } from './mybranch/mybranch.component';
import { TestcomponentComponent } from './testcomponent/testcomponent.component';
import { Component1Component } from './allComponents/component1/component1.component';
import { Component2Component } from './allComponents/component2/component2.component';
import { Pipe1Pipe } from './allpipes/pipe1.pipe';
import { Pipe2Pipe } from './allpipes/pipe2.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponentComponent,
    FourthComponent,
    MybranchComponent,
    TestcomponentComponent,
    Component1Component,
    Component2Component,
    Pipe1Pipe,
    Pipe2Pipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
