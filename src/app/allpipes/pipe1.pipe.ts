import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pipe1'
})
export class Pipe1Pipe implements PipeTransform {

  pipe_var1 :any;

  transform(value:any,): any {

    this.pipe_var1='Pipe one is added.';
    return value+this.pipe_var1;
  }
}
